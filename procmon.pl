#!/usr/bin/env perl
use strict;

use BWC::Config;
use BWC::Util;
use BWC::Queue;

use Proc::Simple;
use Data::Dumper;


my $DEBUG = 0;
my $signal_flag = 0;
my %all_apps;

$SIG{INT} = $SIG{TERM} = $SIG{KILL} =  sub { 
    print "\nGot signal  " . shift() . "\nExiting...\n"; 
    $signal_flag = 1 
};

my @apps = qw( q.pl ip_update.pl );

`sudo rfcomm release all`;

FOREVER:
while (1) {
  my @all_apps = @apps;

  push @all_apps, build_hci_app( list_hcidevices() );

  foreach (@all_apps) {
      if ($all_apps{$_}) {
	  $DEBUG && print "$_ is running already.\n";
      }
      else {
	  $DEBUG && print "$_ is NOT running.\n";
	  $all_apps{$_} = start_app($_);
      }
  }
  
  if ( dead_queue() ) {
      insert_inquiry_task_in_queue(5);
  }

  sleep 30;

  foreach (@all_apps) {
      if ( kill 0, $all_apps{$_}->pid() ){
	  $DEBUG && print "Process id: " . $all_apps{$_}->pid() . 
	    " is running\n";
      }
      else {
	  $DEBUG && print "Process id: " . $all_apps{$_}->pid() . 
	    "is DEAD\n";
	  delete $all_apps{$_};
      }
  }
  last FOREVER if $signal_flag; 
}

#killall

print "Now terminating these processes: " . join ", ", (keys %all_apps);
print "\n\n";
kill 'TERM', map { $_->pid(); } (values %all_apps);


sub start_app {
  my $app = shift;
  my $app_path = "$work_dir/$app";

  $DEBUG && print STDERR "Starting $app_path\n";

  my $process = Proc::Simple->new();
  $process->kill_on_destroy(0);
  $process->start($app_path);

  return $process;
}

sub list_hcidevices {
  return [`hcitool dev` =~ /(hci\d+)/g];
}

sub build_hci_app {
  my $list_of_hcidevices = shift;
  return map { "$_.pl" } @$list_of_hcidevices;
}

sub insert_inquiry_task_in_queue {
  my $delayed_by = shift;
  bwc_trace();

  $delayed_by ||= 10;

  $DEBUG &&
    print "Inserting Inquiry task in queue with delay: $delayed_by\n";

  my $q_rqst = BWC::Queue->new();
  bwc_log("Enqueueing Inquiry Request");
  $q_rqst->enqueue('task=inq;');
}

sub dead_queue {
  my $q = BWC::Queue->new();
  $DEBUG && print "QUEUE SIZE = " .  $q->size . ".\n";
  $DEBUG && print "QUEUE TIME = " .  $q->time . ".\n";
  return ( $q->size < 1 && ( time() - $q->time ) > 3*60 );
}
  


  


