#!/usr/bin/env perl
use strict;
use BWC::Config;
use Proc::Simple;

my $app = "procmon.pl";
my $app_path = "$work_dir/$app";

my $process = Proc::Simple->new();
$process->kill_on_destroy(0);
$process->start($app_path);

open PID, ">${app_path}.pid";
print PID $process->pid();
close PID;






