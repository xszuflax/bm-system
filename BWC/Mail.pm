package BWC::Mail;
use strict;
use LWP::UserAgent;

use BWC::Config;
use BWC::Util;

sub new
{
    bwc_trace();
    my ($arg1, $address, $dbo) = @_;
    my $class = ref($arg1) || $arg1;

    my $ua = LWP::UserAgent->new;
    $ua->agent("BluetoothMarketingSystem/0.1.999");

    my $self = { ua => $ua };

    bless $self => $class;
}

sub send
{
    bwc_trace();
    my ($self, $subj, $msg) = @_;

    my $ua = $self->{ ua };

    my $req = HTTP::Request->new(POST => 'mailto:operator@BluetoothMarketingSystem.com');
    $req->header(Subject => "$subj");
    $req->content($msg);

    my $res = $ua->request($req);

    unless ( $res->is_success ) {
	$self->{ err } = $res->status_line;
	$self->{ res } = $res;
    }
    return $res->is_success;
}

1;
