package BWC::Util;
use BWC::Config;
use Exporter 'import';


sub start_trace {
    my ($trace_filename) = @_;
    open TRACE, ">>$trace_filename";
    select(TRACE); $| = 1;
    select(STDOUT);
    print TRACE timestamp() . "BWC::Util *Tracing Started*";
}

sub whoami {
    return (caller(1))[3];
}

sub bwc_trace {
    return unless -e "$work_dir/trace_on";
    print TRACE timestamp() . (caller(1))[3] . join " ", @_;
}

sub bwc_log {
    print TRACE timestamp() . join " ", @_;
}

sub timestamp {
    my ($sec, $min, $hour, $mday, $mon, $year) = (localtime())[0,1,2,3,4,5];
    #my @months = ( qw| JAN FEB MAR APR MAY JUN JUL AUG SEP OCT NOV DEC | );
    return sprintf("\n%04d%02d%02d|%02d:%02d:%02d|",$year+1900,$mon+1,$mday,
                   $hour,$min,$sec);
}

END {
    if (defined(TRACE)) {
	print TRACE timestamp() . "BWC::Util *Shutting Down*";
	close TRACE;
    }
}

@EXPORT = qw( bwc_log bwc_trace whoami );

1;
