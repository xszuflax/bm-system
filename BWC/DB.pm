package BWC::DB;
use strict;
use DBI;
use Data::Dumper;

use BWC::Config;
use BWC::Util;

sub new
{
    bwc_trace();
    my $arg1 = shift;
    my $class = ref($arg1) || $arg1;

    my $self = {};
    bless $self => $class;
}

sub connect
{
    bwc_trace();
    my ($self, $database, $username, $password) = @_;
    $self->{db_name} = $database;
    $self->{db_uname} = $username;
    $self->{db_passwd} = $password;

    my $dbh = DBI->connect("dbi:Pg:dbname=$database", "$username", "$password",
			   {RaiseError => 1, AutoCommit => 1 });
    $self->{is_connected} = defined($dbh);
    $self->{dbh} = $dbh;
    return $self->{is_connected};
}

sub query_stats_for_admin{
    bwc_trace();
    my ($self) = @_;
 
    my $sql = q| select datesent,key ,count(address) from specials_to_devices group by key,datesent order by datesent; |; 
    return $self->{dbh}->selectall_hashref($sql, 'datesent');
}

sub disconnect
{
    bwc_trace();
    my ($self) = @_;
    if ($self->{is_connected}) { 
	$self->{is_connected} = 0;
	return $self->{dbh}->disconnect();
    }
}

sub insert_device
{
    bwc_trace();
    my ($self, $address, $channel) = @_;
    return unless ( defined($address) && defined($channel) );
    my $sql = qq| INSERT INTO devices(address, channel, added) VALUES ('$address', '$channel', current_date) |;
    return $self->{dbh}->do($sql);
}

sub update_device_is_informed
{
    bwc_trace();
    my ($self, $address) = @_;
    return unless defined($address);
    my $sql = qq| UPDATE devices SET informed = true WHERE address = '$address' |;
    return $self->{dbh}->do($sql);
}

sub get_device
{
    bwc_trace();
    my ($self, $address) = @_;
    return unless defined($address);
    my $sql = qq| SELECT address, channel, informed, added FROM devices WHERE address = '$address' |;
    return $self->{dbh}->selectall_hashref($sql, 'address');
}

sub insert_incompatible
{
    bwc_trace();
    my ($self, $address) = @_;
    return unless defined($address);
    my $sql = qq| INSERT INTO incompatibles(address, attempts) VALUES ('$address', 1) |;
    return $self->{dbh}->do($sql);
}

sub update_incompatible
{
    bwc_trace();
    my ($self, $address) = @_;
    return unless defined($address);
    my $sql = qq| UPDATE incompatibles SET attempts = attempts + 1 WHERE address = '$address' |;
    return $self->{dbh}->do($sql);
}

sub get_incompatible
{
    bwc_trace();
    my ($self, $address) = @_;
    return unless defined($address);
    my $sql = qq| SELECT address, attempts FROM incompatibles WHERE address = '$address' |;
    return $self->{dbh}->selectall_hashref($sql, 'address');
}

sub insert_special
{
    bwc_trace();
    my ($self, $filename, $startdate, $starttime, $enddate, $endtime, $hciname, $master_key) = @_;
    my $sql = qq| INSERT INTO specials(filename, startdate, starttime, enddate, endtime, hciname) VALUES ('$filename', '$startdate', '$starttime', '$enddate', '$endtime', '$hciname') |;
    my $status = $self->{dbh}->do($sql);
    my $curval_sql = qq| SELECT currval('seq_specials') |;
    return $self->{dbh}->selectall_arrayref($curval_sql)->[0][0];
}

sub update_special
{
    bwc_trace();
    my ($self, $key, $field_value) = @_;
    print Dumper $field_value;
    my %allowed = ( filename => 1,
		    startdate => 1,
		    enddate => 1,
		    starttime => 1,
		    endtime => 1,
		    hciname => 1 );
    
    my $num_of_fields = scalar keys %{$field_value};
    unless ($num_of_fields) { return -1; }

    my $sql_fields_values = "";

    foreach my $field (keys %{$field_value}) {
	if (defined($allowed{$field})) {
	    $sql_fields_values .= "$field = \'" . $field_value->{$field} . "\',";
	}else{
	    print "-->$field<--";
	    return -2;
	}
    }
    chop($sql_fields_values);
    my $sql = qq| UPDATE specials SET $sql_fields_values WHERE key = $key |;

    return $self->{dbh}->do($sql);
}

sub get_current_specials
{
    bwc_trace();
    my ($self) = @_;
    my $sql = qq| SELECT key, filename, startdate, starttime, enddate, endtime FROM specials WHERE startdate <= current_date AND enddate >= current_date AND starttime <= current_time AND endtime >= current_time |;
    return $self->{dbh}->selectall_hashref($sql, 'key');
}

sub get_current_specials_not_sent_to_phone
{
    bwc_trace();
    my ($self, $address) = @_;
    return unless defined($address);
    my $sql = qq| SELECT key, filename, startdate, starttime, enddate, endtime, hciname FROM specials WHERE startdate <= current_date AND enddate >= current_date AND starttime <= current_time AND endtime >= current_time AND key not in ( select key from specials_to_devices where address = '$address' ) |;
    return $self->{dbh}->selectall_hashref($sql, 'key');
}


sub get_special_by_key
{
    bwc_trace();
    my ($self, $key) = @_;
    return unless defined($key);
    my $sql = qq| SELECT key, filename, startdate, starttime, enddate, endtime, hciname, master_key FROM specials WHERE key = $key |;
    return $self->{dbh}->selectall_hashref($sql, 'key');
}

sub get_special_by_master_key
{
    bwc_trace();
    my ($self, $master_key) = @_;
    return unless defined($master_key);
    my $sql = qq| SELECT key, filename, startdate, starttime, enddate, endtime, hciname, master_key FROM specials WHERE master_key = $master_key |;
    return $self->{dbh}->selectall_hashref($sql, 'master_key');
}

sub insert_discovered
{
    bwc_trace();
    my ($self, $address) = @_;
    return unless defined($address);
    my $sql = qq| INSERT INTO discovered(address, date, time) VALUES ('$address', current_date, current_time) |;
    return $self->{dbh}->do($sql);
}

sub insert_special_to_device
{
    bwc_trace();
    my ($self, $address, $special_key) = @_;
    return unless defined($address);
    return unless defined($special_key);
    my $sql = qq| INSERT INTO specials_to_devices(address, key, datesent, timesent) VALUES ('$address', '$special_key', current_date, current_time) |;
    return $self->{dbh}->do($sql);
}

sub remove_special_to_device
{
    bwc_trace();
    my ($self, $address, $special_key) = @_;
    return unless defined($address);
    return unless defined($special_key);
    my $sql = qq| DELETE FROM specials_to_devices WHERE address = '$address' AND key = '$special_key' |;
    return $self->{dbh}->do($sql);
}

sub get_special_to_device
{
    bwc_trace();
    my ($self, $address, $special_key) = @_;
    return unless defined($address);
    return unless defined($special_key);
    my $sql_where = "WHERE ";
    if (defined($address) && defined($special_key)) {
	$sql_where .= "address = '$address' AND ";
	$sql_where .= "key = '$special_key'";
    }elsif (defined($address)) {
	$sql_where .= "address = '$address'";
    }elsif (defined($special_key)) {
	$sql_where .= "key = '$special_key'";
    }else{
	return {};
    }

    my $sql = qq| SELECT address, key, datesent, timesent FROM specials_to_devices $sql_where |;
    return $self->{dbh}->selectall_arrayref($sql);
}

sub get_last_datesent_to_device
{
    bwc_trace();
    my ($self, $address) = @_;
    return unless defined($address);
    my $sql = qq| SELECT address, max(datesent) AS max_datesent FROM specials_to_devices WHERE address = '$address' GROUP BY address |;
    return $self->{dbh}->selectall_hashref($sql,'address');
}

sub get_last_timesent_to_device
{
    bwc_trace();
    my ($self, $address) = @_;
    return unless defined($address);
    my $sql = qq|SELECT address, datesent AS last_datesent, max(timesent) AS last_timesent FROM specials_to_devices WHERE address = '$address' AND datesent IN (SELECT max(datesent) AS datesent FROM specials_to_devices WHERE address = '$address') GROUP BY datesent, address |;
    return $self->{dbh}->selectall_hashref($sql,'address');
}

sub get_config
{
    bwc_trace();
    my ($self, $key) = @_;
    return unless defined($key);
    my $sql = q| SELECT key, value FROM config |;
    my $hashref = $self->{dbh}->selectall_hashref($sql,'key');
    
    if (defined($hashref->{$key})) {
	return $hashref->{$key}->{value};
    }else{
	return undef;
    }
}

sub set_config
{
    bwc_trace();
    my ($self, $key, $value) = @_;
    return unless defined($key);
    return unless defined($value);

    if (defined($self->get_config($key))) {
	my $sql = qq| UPDATE config SET value = '$value' WHERE key = '$key' |;
	return $self->{dbh}->do($sql);
    }
}


1;
