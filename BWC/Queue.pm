package BWC::Queue;

use LWP::UserAgent;
use Data::Dumper;

sub new {
  my $class = shift;

  my $ua = new LWP::UserAgent;
  $ua->agent("funQ" . $ua->agent);

  bless { status => 'active', 
	  ua => $ua }, $class;
}

sub enqueue {
  my ($self, $what) = @_;

  my $req = new HTTP::Request GET => "http://localhost:39393/xENQUEUEx$what";
  my $res = $self->{ua}->request($req);
  
  return 1;
}

sub dequeue {
  my ($self) = @_;
  
  my $req = new HTTP::Request GET => "http://localhost:39393/xDEQUEUEx";
  my $res = $self->{ua}->request($req);

  if ($res->is_success) {
    my ($status) = ($res->as_string =~ /deq-(n*ok)+/);
    if ($status =~ /^ok/) { return $res->content(); }
  }
  return undef;
}

sub size {
  my ($self) = @_;
  
  my $req = new HTTP::Request GET => "http://localhost:39393/xQSIZEx";
  my $res = $self->{ua}->request($req);

  return $res->content() if $res->is_success;
}

sub time {
  my ($self) = @_;
  
  my $req = new HTTP::Request GET => "http://localhost:39393/xQTIMEx";
  my $res = $self->{ua}->request($req);

  return $res->content() if $res->is_success;

}

1;
