package BWC::Phone;
use strict;
use Data::Dumper;

use BWC::Config;
use BWC::Util;
use BWC::DB;
use BWC::Rfcomm;

sub new
{
    bwc_trace();
    my ($class, $address, $channel, $dbo) = @_;

    die "Wrong address format: $address" 
	unless $address =~ /(?:[\da-f][\da-f]:){5}/i;

    die "No database object" unless defined($dbo);

    my $self = { address => $address, dbo => $dbo, channel => $channel  };
    my $dev = $dbo->get_device($address);

    if ( defined($dev->{$address}) ) {
	foreach my $k (keys %{$dev->{$address}}) {
	    $self->{$k} = $dev->{$address}->{$k};
	}
    }else{
	if (defined($channel)) {
	    $self->{dbo}->insert_device($self->{address}, $self->{channel});
	}
    }
    bless $self => $class;
}

sub get_address
{
    bwc_trace();
    my ($self) = @_;
    return $self->{address};
}

sub is_admin {
    my $self = shift;
    my $address = $self->get_address();

    my %admins = (
                  '00:11:9f:c4:f7:a7' => 1,
                  );

    return exists($admins{$address});
}

sub get_last_datesent
{
    bwc_trace();
    my ($self) = @_;
    return $self->{dbo}->get_last_datesent_to_device($self->{address});
}

sub get_last_timesent
{
    bwc_trace();
    my ($self) = @_;
    return $self->{dbo}->get_last_timesent_to_device($self->{address});
}

sub is_informed
{
    bwc_trace();
    my ($self) = @_;
    return $self->{informed} == 1;
}

sub inform
{
    bwc_trace();
    my ($self) = @_;
    if ($self->send_by_key(1)) {
	$self->{dbo}->update_device_is_informed($self->{address});
    }
}

sub is_incompatible
{
    bwc_trace();
    my ($self) = @_;
    my $dbo = $self->{dbo};
    my $address = $self->{address};

    return 'incompatible' if defined( $self->{incompatible} );

    my $attempts = $dbo->get_incompatible($address);

    my $result;
    if ( defined($attempts->{$address}) 
	 && $attempts->{$address}->{attempts} > $MAX_OPUSH_ATTEMPTS ) 
    {
	$self->{incompatible} = 1;
	$result = 'incompatible';
    }
    
    return $result;
}

sub get_opush_channel
{
    bwc_trace();
    my ($self) = @_;

    return $self->{channel} if defined($self->{channel});

}

sub get_available_specials
{
    bwc_trace();
    my ($self) = @_;
    return $self->{dbo}->get_current_specials_not_sent_to_phone($self->{address});
}


sub special_sent
{
    my ($self, $special_key) = @_;
    return unless defined($special_key);

    $self->{dbo}->insert_special_to_device($self->{address}, 
					   $special_key );
    return;
}

  
sub make_incompatible
{
    my ($self) = @_;

    # make incompatible is for signing out
    # TODO

}

1;
