package BWC::Config;

use Exporter 'import';

$work_dir = '/opt/bm-system';

$temp_dir = "/tmp";

$MINIMUM_DAYS_BETWEEN_MESSAGES = 7;

$MINIMUM_MINUTES_BETWEEN_MESSAGES = 5;

$MAX_OPUSH_ATTEMPTS = 20;

$HOST_REF = '999';

$BWCDB='bluemarketing';

@EXPORT = qw( $work_dir  $temp_dir $MINIMUM_MINUTES_BETWEEN_MESSAGES $MINIMUM_DAYS_BETWEEN_MESSAGES $MAX_OPUSH_ATTEMPTS $HOST_REF $BWCDB ); 

1;
