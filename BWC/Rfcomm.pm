package BWC::Rfcomm;
use strict;
use Time::HiRes "usleep";
use Expect;
use Data::Dumper;

use BWC::Util;
use BWC::Config;

our $rfcomm_array;
our $initialised = 0;
our $work_dir = ${BWC::Config::work_dir};

sub new
{
    bwc_trace();

    my ($arg, $hci_device) = @_;
    my $class = ref($arg) || $arg;
    my $range;

    unless ($hci_device) { $hci_device = 'hci0'; }
    ($range) = ($hci_device =~ /hci(\d+)/);
    die "hci device is not looking good: $hci_device\n"	unless defined $range;
    unless ($initialised) { $rfcomm_array = _init_rfcomm_array($range); }

    my $rfcomm_device = pop @{$rfcomm_array};
    my $self = { device => $rfcomm_device, hci_device => lc($hci_device) };
    bless $self => $class;
}

sub bind
{
    bwc_trace();
    my ($self, $address, $channel) = @_;

    my $device = $self->{ device };
    my $hci_dev_param = "-i " . $self->{ hci_device };

#checks if not already connected to address
    my $connections = `rfcomm`;
    if ($connections =~ /$address/gi) {
	bwc_log("$address already bound");
	return 0;
    }

    my $status = `sudo rfcomm $hci_dev_param bind $device $address $channel`;
    if ($status) { 
	bwc_log $status;
	return 0;
    }
    bwc_log("will use $device for $address:$channel");
    while (! -e $device) {
	#sleep 1/3 of a second until the device is ready
	usleep 30_000;
    }
    $self->{bound} = 1;

    $connections = `rfcomm`;
    my @cumul;
    push @cumul, ($connections =~ /($address)/gmi);
    unless (@cumul == 1){
	$self->release;
	return 0;
    }

    return 1;
}

sub connect
{
    bwc_trace();
    my ($self, $address, $channel) = @_;

    my $device = $self->{ device };
    my $hci_dev_param = "-i " . $self->{ hci_device };

    my $status = `sudo rfcomm $hci_dev_param connect $device $address $channel`;
    if ($status) { bwc_log $status; }
}

sub release
{
    bwc_trace();
    my ($self) = @_;
    return unless defined($self->{bound});
    my $device = $self->{ device };
    my $status = `sudo rfcomm release $device`;
    if ($status) { bwc_log $status; }
    $self->{bound} = undef;
    return $status;
}

sub push
{
    bwc_trace();
    my ($self, $local_file, $remote_file, $timeout) = @_;
    my $device = $self->{ device };

    my $command = "${work_dir}/opush";
    my @params = ($device, $local_file, $remote_file);

    my $exp = new Expect;
    $exp->raw_pty(1);  
    $exp->spawn($command, @params) || die "Could not spawn $command\n$!";

    $self->{has_succedeed} = 0;
    $self->{has_failed} = 0;

    $exp->expect($timeout,
		 [ "^nok:", \&_give_up_push, @_ ],
		 [ "rsp: 43", \&_give_up_push, @_ ],
		 [ "rsp: 41", \&_give_up_push, @_ ],
		 [ "^ok: Pushed", \&_push_succedeed, @_ ],
		 );
    
    my $rtrn = !$self->{has_failed} && $self->{has_succedeed};
    return $rtrn;
}

sub _push_succedeed
{
    bwc_trace();
    my ($expect_obj, $self, @params) = @_; 
    $self->{has_succedeed} = 1;
}

sub _give_up_push
{
    bwc_trace();
    my ($expect_obj, $self, @params) = @_; 
    $expect_obj->soft_close();    
    $self->{has_succedeed} = 0;
    $self->{has_failed} = 1;
}

sub _init_rfcomm_array {
    my ($range) = @_;
    if ($range == 0) { $range = undef; }
    my @ary;
    for (0..9) {
	push @ary, "/dev/rfcomm${range}$_";
    }
    return \@ary;
}

sub DESTROY
{
    bwc_trace();
    my ($self) = @_;
    push @{$rfcomm_array}, $self->{ device };
}

1;
