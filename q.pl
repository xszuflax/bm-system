#!/usr/bin/env perl
use strict;
use warnings;
use HTTP::Daemon;
use HTTP::Response;
use vars qw($VAR1);
use Data::Dumper;

main();

sub main
{
  my @queue;
  my $d = HTTP::Daemon->new(ReuseAddr=>1, LocalPort => 39393) || 
    die "Can't start daemon. $!";

  my $tracker = 0;
  my $resp;

  print "Started Bluetooth Marketing HTTP Queueing System.\n";
  while (my $c = $d->accept) {
	while (my $r = $c->get_request) {
	  my $req_str = $r->as_string;

	  if ( $req_str =~ /xENQUEUEx(.*)HTTP/ ) {
	    push @queue, $1;

	    if ($1 =~ /inq/) {
#	      print "\n** QUEUE>  Inquiry detected, tracking\n";
	      $tracker = time();
	    }

	    $resp = new HTTP::Response(200, "enq-ok");
	    $resp->content("enq-ok");
	  }
	  elsif ( $req_str =~ /xDEQUEUEx/ ) {
	    my $elem = shift @queue;
	    
	    $resp = new HTTP::Response(200,
				       defined($elem)?
				       "deq-ok:<data>$elem</data>":
				       "deq-nok");
	    $resp->content($elem);
	  }
	  elsif ( $req_str =~ /xQSIZEx/ ){
	    $resp = new HTTP::Response(200);
	    $resp->content(scalar @queue);
	  }
	  elsif ( $req_str =~ /xQTIMEx/ ){
	    $resp = new HTTP::Response(200);
	    $resp->content($tracker);
	  }
	  else {
	    $resp = new HTTP::Response(404);
	  }
	}
	$c->send_response($resp);
	$c->close();
	undef($c);
      }
}

