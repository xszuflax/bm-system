#!/usr/bin/env perl

use BWC::Config;
use LWP::UserAgent;
$ua = new LWP::UserAgent;
$ua->agent("ip_tracker/1.0" . $ua->agent);

# Create a request
my $req = new HTTP::Request GET => "http://BluetoothMarketingSystem.com:36395/bm-live-$HOST_REF";

while(1){
# Pass request to the user agent and get a response back
    my $res = $ua->request($req);

# Check the outcome of the response
    if (! $res->is_success) {
      open LOGFILE, ">>update_ip.log";
      print LOGFILE localtime() . " Could not update IP\n";
      close LOGFILE;
    }
    sleep 3*60;
}

