#!/usr/bin/perl
use strict;
use LWP::UserAgent;
use File::Temp qw/ tempdir /;
use File::Copy;

use BWC::Config;
use BWC::DB;
use BWC::Mail;
use BWC::Util;

my $dbo = new BWC::DB();
$dbo->connect($BWCDB);

my $ua = LWP::UserAgent->new;
my $mail = new BWC::Mail;

my $timestamp;
my $temp_dir = tempdir( CLEANUP => 1 );

$ua->agent("BluetoothMarketingSystem/0.1.999");

my $ts_req = HTTP::Request->new(GET => "http://BluetoothMarketingSystem.com/updates/bm-live-${HOST_REF}.ts");
my $ts_res = $ua->request($ts_req);

if ( $ts_res->is_success ) {
    if ( $ts_res->content =~ /^(\d{14})$/ ) {
	$timestamp = $1;
    }else{
	$mail->send('ERROR detected in update.pl', "wrong timestamp format : " . $ts_res->content);
	exit(-1);
    }
}
else
{
    $mail->send('ERROR detected in update.pl', $ts_res->status_line);
    exit(-1);
}

exit(1) unless timestamp_newer($timestamp);

my $upd_req = HTTP::Request->new(GET => "http://BluetoothMarketingSystem.com/updates/bm-live-${HOST_REF}.upd");
my $upd_res = $ua->request($upd_req);

my %specials;

if ( $upd_res->is_success ) {
    my @rows = split "\n",$upd_res->content;
    foreach my $row (@rows) {
	my ($filename, $startdate, $starttime, $enddate, $endtime) = 
	    split /\|/, $row;
	unless (  $filename =~ /^[\w\d\.]+/ ) {
	    $mail->send('ERROR detected in update.pl', "problem with filename $filename");
	    exit(-1);
	}
	unless ( $startdate =~ /^\d{8}/ && $enddate =~ /^\d{8}/ ) { 
	    my $msg = "problem with start date or end date\n";
	    $msg .= "start date: $startdate\nend date: $enddate\n";
	    $mail->send('ERROR detected in update.pl', $msg);
	    exit(-1);
	}
	unless ( $starttime =~ /^\d{2}:\d{2}:\d{2}$/ && 
		 $endtime =~ /^\d{2}:\d{2}:\d{2}$/ ) {
	    my $msg = "problem with start time or end time\n";
	    $msg .= "start time: $starttime\nend time: $endtime\n";
	    $mail->send('ERROR detected in update.pl', $msg);
	    exit(-1);
	}
	#Looks good enough
	$specials{$filename} = { startdate => $startdate,
				 starttime => $starttime,
				 enddate => $enddate,
				 endtime => $endtime, };
    }
}
else
{
    $mail->send('ERROR detected in update.pl', $upd_res->status_line);
    exit(-1);
}
    
foreach my $file (keys %specials) {
    my $url = "http://BluetoothMarketingSystem.com/updates/bm-live-${HOST_REF}/$timestamp/$file";
    my $file_req = HTTP::Request->new(GET => $url);
    my $file_res = $ua->request($file_req);
    
    if ( $file_res->is_success ) {
	my $open_status = open (FILE, ">$temp_dir/$file");
	unless ( $open_status ) { 
	    $mail->send('ERROR detected in update.pl', "Could not open $temp_dir/$file for writing");
	    exit(-1);
	}
	print FILE $file_res->content;
	close FILE;
    }
    else
    {
	$mail->send('ERROR detected in update.pl', $upd_res->status_line);
	exit(-1);
    }
}

my $msg_success = "";
foreach my $file (keys %specials) {
    my $startdate = $specials{$file}->{startdate};
    my $starttime = $specials{$file}->{starttime};
    my $enddate = $specials{$file}->{enddate};
    my $endtime = $specials{$file}->{endtime};
    my $special_key =$dbo->insert_special($file,$startdate,$starttime,
					  $enddate, $endtime);
    move("$temp_dir/$file", "${work_dir}/data/$special_key");
    $msg_success .= "$special_key|$file|$startdate|$starttime|$enddate|$endtime\n";
}
$mail->send('SUCCESS', $msg_success);

update_timestamp($timestamp);
 

sub timestamp_newer
{
    my ($timestamp) = @_;
    my $last_timestamp = $dbo->get_config('last_timestamp');

    return ( $timestamp > $last_timestamp );
}

sub update_timestamp
{
    my ($timestamp) = @_;
    $dbo->set_config('last_timestamp', $timestamp);
}
