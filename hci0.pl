#!/usr/bin/env perl
use strict;
use Date::Simple qw/ date today /;
use Time::HiRes "usleep";
use Time::Local;

use BWC::Config;
use BWC::Phone;
use BWC::Rfcomm;
use BWC::Util;
use BWC::DB;
use BWC::Queue;

use Data::Dumper;

my ($HCI) = ($0 =~ /(hci[\d]+).pl/);

BWC::Util::start_trace("${work_dir}/log/$HCI.log");

my $queue = new BWC::Queue;

my $dbo = new BWC::DB;
$dbo->connect($BWCDB);

while(1)
{
    my $elem = $queue->dequeue();
    unless ($elem) {
	sleep 1;
	next;
    }
    my $status;

    my %job = split /[=;]/, $elem;
    print "$HCI> job:$job{task}\n";

    if( $job{task} eq 'scan' )
    {
	_perform_scan($queue);
	_request_new_scan($queue);
    }
    elsif( $job{task} eq 'inq' )
    {
	_perform_inq($queue);
	_request_new_inq($queue);
	sleep 2;
    }
    elsif( $job{task} eq 'serv' )
    {
	_perform_service_search(\%job, $dbo);
    }
    elsif( $job{task} eq 'msg' )
    {
	_perform_send_message($queue, \%job, $dbo);
    }
    elsif( $job{task} eq 'fire' )
    {
	_perform_fire_message(\%job);

    }
    else
    {
	bwc_log("**   Unsupported Task   **"); 
	bwc_log Dumper(\%job);

    }
}

sub _request_new_scan
{
    my ($queue) = @_;
    $queue->enqueue("task=scan;");
}
sub _request_new_inq
{
    my ($queue) = @_;
    $queue->enqueue("task=inq;");
}
sub _perform_service_search
{
    bwc_trace();

    my ($job, $dbo) = @_;
    return unless defined($job->{address});

    my $address = $job->{address};

    open(SERVICE, "sdptool -i $HCI search --bdaddr $address OPUSH|") ||
      print 'Could not proceed with sdptool search';
    my $service = do{ local $/; <SERVICE> };
    close(SERVICE);

    my ($opush_channel) = ($service =~ /Channel:\s*(\d+)/);

    if ($opush_channel) {
      my $phone = BWC::Phone->new($address, $opush_channel, $dbo);
    }
    #
    # else look into incompatibility
    #

    close(SERVICE);
    return;
}

sub _perform_send_message
{
    bwc_trace();

    my ($queue, $job, $dbo) = @_;
    return unless defined($job->{address});

    my $address = $job->{address};

    my $phone = new BWC::Phone($address, undef, $dbo);

    my $channel = $phone->get_opush_channel;

    unless ($channel) {
	_perform_service_search($job, $dbo);
    }

    if ( $phone->is_admin() ){
	send_stats_to_admin($phone);
    }

    my $lastsent = $phone->get_last_timesent;

    if (defined($lastsent->{$address}))
    {
	my $lastdate = $lastsent->{$address}->{last_datesent};
	my $lasttime = $lastsent->{$address}->{last_timesent};
	unless (gap_between_specials_ok_time($lastdate, $lasttime))
	{
	    # bwc_log "too soon to re-spam $address";
	    return;
	}
    }
    else
    {
	# bwc_log "$address hasn't received any of our messages";
    }

    my $specials_avail = $phone->get_available_specials;
    if (scalar(keys %$specials_avail) > 0)
    {
	my @keys_avail = keys %$specials_avail;
	my $random_key = $keys_avail[int(rand(@keys_avail))];
	my $chosen_special = $specials_avail->{$random_key};

	my $rfcomm = new BWC::Rfcomm($HCI);

	if ($rfcomm->bind($address, $channel))
	{
	    my $timeout = 120;
	    my $remote_file = $chosen_special->{filename};
	    my $local_file = "${work_dir}/data/" . $chosen_special->{key};
	    return unless -e $local_file;

	    my $hciname = $chosen_special->{hciname};

	    rename_hcidevice($HCI, $hciname);

	    my $status = $rfcomm->push($local_file, $remote_file, $timeout);
	    if ($status) 
	    {
		$phone->special_sent($chosen_special->{key});
		usleep(50_000);
	    }
	    else
	    {
		bwc_log("failed while attempting to send special " . $chosen_special->{key});
	    }
	}
	$rfcomm->release();
    }
    else 
    {
	# bwc_log "nothing to send to $address";
	return;
    }

    return;
}

sub send_stats_to_admin {
    my $phone = shift;

    my $address = $phone->get_address();
    my $channel = $phone->get_opush_channel();
    my $remote_file = 'stats.txt';
    my $local_file = '/tmp/bwc_stats.txt';

    open STATS_FILE, ">$local_file" || bwc_log "CAN'T OPEN $local_file !?";
    my $stats = $dbo->query_stats_for_admin();

    foreach (sort keys %$stats) {
	print STATS_FILE "$_ ($stats->{$_}{key}) = $stats->{$_}{count}\n";
    }

    print STATS_FILE "\n--UPTIME--\n" . `uptime` . "\n\n";
    print STATS_FILE "\n--DISK SPACE--\n" . `df` . "\n\n";
    print STATS_FILE "\n--PROCESSES--\n" . `ps xf` . "\n\n";
    print STATS_FILE "\n--HCI DEVICES\n" . `hcitool dev` . "\n\n";

    close STATS_FILE;

    return _perform_fire_message($address, $channel, $local_file, $remote_file);
}

sub _perform_fire_message
{
    bwc_trace();

    my ($address, $channel, $local_file, $remote_file) = @_;

    return unless ( -e $local_file );

    my $rfcomm = new BWC::Rfcomm($HCI);
    my $status;
    my $attempts = 1;
    # two attempts before giving up
    while ( !$status && $attempts<3 )
    {
	if ($rfcomm->bind($address, $channel))
	{
	    my $timeout = 120;
	    $status = $rfcomm->push($local_file, $remote_file, $timeout);
	}
	$rfcomm->release();
	$attempts += 1;
    }

    return;
}

sub _perform_inq
{
    bwc_trace();
    my ($queue) = @_;

    reset_hcidevice();

    open(PIPE, "hcitool -i $HCI inq --flush|") or
	die 'cannot open hcitool';

    my $task='msg';
    my $address;

    while(<PIPE>)

    {
	if (/([\da-fA-F:]{17})/){
	    $address = lc($1);
	    $queue->enqueue("task=$task;address=$address;");
	}
	elsif (/failed/){
	  bwc_log("** INQUIRY FAILED **");
	  reset_hcidevice();
	}
	  
    }
    close(PIPE);
    return;
}

sub _perform_scan
{
    bwc_trace();
    my ($queue) = @_;

    reset_hcidevice();

    open(PIPE, "sdptool -i $HCI search OPUSH|") || 
	die 'cannot open sdptool';
    my @scanned;
    my ($task, $address, $channel);
    $task = 'msg';

    # bwc_log "scan starts at " . localtime();

    while(<PIPE>)
    {
      if (m/([\da-fA-F:]{17})/){
	$address = lc($1);
      }elsif(m/Channel:\s+(\d+)/){
	$channel = $1;
	$queue->enqueue("task=$task;channel=$channel;address=$address;");
      }
    }
    # bwc_log "scan ends at " . localtime();
    close(PIPE);
    return;
}


sub rename_hcidevice
{
    my ($dev, $name) = @_;
    return unless $dev && $name;
    my $status = `sudo hciconfig $dev name $name`;
    if ($status) {
	print STDERR $status;
    }
	
}
sub reset_hcidevice
{
    my $status = `sudo hciconfig $HCI reset`;
    if ($status) {
	bwc_log $status;
    }
    sleep 2;
}
sub gap_between_specials_ok
{
    
    my ($date) = @_;
    return ( (today() - date($date)) > $MINIMUM_DAYS_BETWEEN_MESSAGES );
}

sub gap_between_specials_ok_time
{
    
    my ($date, $time) = @_;

    my ($h, $m, $s) = ($time =~ /(\d+)\D(\d+)\D(\d+)/i);
    my ($Y, $M, $D) = ($date =~ /(\d+)\D(\d+)\D(\d+)/i);

    my $seconds = timelocal($s, $m, $h, $D, $M-1, $Y-1900);
    my $diff = (time() - $seconds)/60;
    return $diff > $MINIMUM_MINUTES_BETWEEN_MESSAGES;
}

